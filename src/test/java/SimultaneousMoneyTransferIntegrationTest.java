import com.example.dto.TransferPost;
import com.example.web.DaggerWebApp;
import com.google.gson.Gson;
import helper.CircularIterableWrapper;
import lombok.SneakyThrows;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.example.dao.DbModule.*;
import static java.net.HttpURLConnection.HTTP_OK;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class SimultaneousMoneyTransferIntegrationTest {

    private static final BigDecimal TOTAL_USD_IN_BANK = new BigDecimal("1290.00");

    private static Gson gson = new Gson();

    private static CircularIterableWrapper<String> transferRequests;

    @BeforeClass
    public static void setUp() {
        DaggerWebApp.create()
                    .router()
                    .start();

        List<String> requests = new ArrayList<>();
        requests.add(prepareJson(1, 3, "9.99"));
        requests.add(prepareJson(3, 5, "9.99"));
        requests.add(prepareJson(5, 1, "9.99"));
        transferRequests = new CircularIterableWrapper<>(requests);
    }

    private static String prepareJson(long from, long to, String amnt) {
        return gson.toJson(new TransferPost(from, to, new BigDecimal(amnt)));
    }

    @Test
    public void testSimultaneousInvocations() throws InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(2);
        pool.execute(new TxReqRunner(transferRequests));
        pool.execute(new TxReqRunner(transferRequests));

        int checksLeft = 7;
        while (checksLeft != 0) {
            checksLeft--;
            TimeUnit.SECONDS.sleep(2);
            assertThat(getUsdSumInBank(), is(equalTo(TOTAL_USD_IN_BANK)));
        }
    }

    @SneakyThrows
    private BigDecimal getUsdSumInBank() {
        try (Connection conn = DriverManager.getConnection(JDBC_URL, H2_DEFAULT_USER, H2_DEFAULT_PASSWORD);
             PreparedStatement ps = conn.prepareStatement("select sum(BALANCE) as USD_SUM from ACCOUNT where CURRENCY = 'USD'")
        ) {
            try (ResultSet rs = ps.executeQuery()) {
                rs.next();
                return rs.getBigDecimal("USD_SUM");
            }
        }
    }
}

class TxReqRunner implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(TxReqRunner.class);

    private final Iterable<String> requests;
    private final CloseableHttpClient client = HttpClients.createDefault();

    TxReqRunner(Iterable<String> requests) {
        this.requests = requests;
    }

    @Override
    public void run() {
        for (String reqJson : requests) {
            boolean executedOk = false;
            while (!executedOk) {
                executedOk = sendReq(reqJson);
            }
        }
    }


    @SneakyThrows
    private boolean sendReq(String reqJson) {
        HttpPost req = new HttpPost("http://localhost:4567/transfer");
        req.setHeader("Accept", "application/json");
        req.setHeader("Content-type", "application/json");
        req.setEntity(new StringEntity(reqJson));
        CloseableHttpResponse resp = client.execute(req);
        log.info(new BasicResponseHandler().handleResponse(resp));
        return resp.getStatusLine().getStatusCode() == HTTP_OK;
    }
}
