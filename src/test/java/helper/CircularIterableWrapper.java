package helper;

import java.util.Iterator;

public class CircularIterableWrapper<T> implements Iterable<T> {
    private final Iterable<T> it;

    public CircularIterableWrapper(Iterable<T> it) {
        this.it = it;
    }

    @Override
    public Iterator<T> iterator() {
        return new CircularIterator();
    }

    private class CircularIterator implements Iterator<T> {
        Iterator<T> i = it.iterator();

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public synchronized T next() {
            while (!i.hasNext()) {
                i = it.iterator();
            }
            return i.next();
        }
    }
}
