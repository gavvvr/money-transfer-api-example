-- User
create table user
(
  user_id long auto_increment,
  constraint user_pk
    primary key (user_id),

  user_name varchar2(100) not null
);

-- Account
create table account
(
  account_id long auto_increment,
  constraint account_pk
    primary key (account_id),
  user_id long not null,
  constraint account_USER_ID_fk
    foreign key (user_id) references USER (user_id),
  account_name varchar2(100) not null,

    balance decimal not null default 0,
  currency varchar2(5) not null,
  constraint chk_Currency check
    (currency IN ('USD', 'RUB', 'EUR'))
);

-- Transaction history
create table transfer_log
(
  from_id long not null,
  constraint tx_from_account_id_fk
    foreign key (from_id) references account (account_id),
  to_id long not null,
  constraint tx_to_account_id_fk
    foreign key (to_id) references account (account_id),
  amount decimal not null,
  stamp timestamp not null
);

create index TRANSFER_LOG_FROM_ID_STAMP_index
  on transfer_log (from_id, stamp);
create index TRANSFER_LOG_TO_ID_STAMP_index
  on transfer_log (to_id, stamp);
