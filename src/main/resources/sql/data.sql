insert into USER (USER_ID, USER_NAME)
values (1, 'fooUser'),
       (2, 'barUser'),
       (3, 'bazUser');

insert into ACCOUNT (USER_ID, ACCOUNT_ID, ACCOUNT_NAME, CURRENCY, BALANCE)
values (1, 1, 'MyUsd', 'USD', 42),
       (1, 2, 'MyRub', 'RUB', 420),

       (2, 3, 'USD-account', 'USD', 148),
       (2, 4, 'EUR-account', 'EUR', 40),

       (3, 5, 'spend', 'USD', 100),
       (3, 6, 'saving', 'USD', 1000);

insert into TRANSFER_LOG (FROM_ID, TO_ID, AMOUNT, STAMP)
values (1, 3, 8, parsedatetime('01-10-2018 10:51:52.690', 'dd-MM-yyyy hh:mm:ss.SS'));
