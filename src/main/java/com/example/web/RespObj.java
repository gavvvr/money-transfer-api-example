package com.example.web;

public class RespObj {
    public final Result status;
    public final Object payload;
    public final String message;

    public RespObj(Result status, Object payload) {
        this.status = status;
        this.payload = payload;
        this.message = null;
    }

    public RespObj(Result status, String message) {
        this.status = status;
        this.message = message;
        this.payload = null;
    }

    public enum Result {
        SUCCESS, ERROR,
    }
}
