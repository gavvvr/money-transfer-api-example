package com.example.web;

import com.example.dao.DbModule;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = DbModule.class)
public interface WebApp {
    Router router();
}
