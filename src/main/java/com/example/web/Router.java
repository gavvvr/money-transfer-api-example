package com.example.web;

import com.example.controller.AccountController;
import com.example.controller.TransferController;
import com.example.controller.UserController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import static com.example.Utils.GSON;
import static com.example.web.RespObj.Result.ERROR;
import static spark.Spark.*;

public class Router {

    private static final Logger log = LoggerFactory.getLogger(Router.class);
    private static final String MIME_JSON = "application/json";

    private UserController userController;
    private AccountController accountController;
    private TransferController transferController;

    @Inject
    public Router() {
    }

    @Inject
    public void setUserController(UserController userController) {
        this.userController = userController;
    }

    @Inject
    public void setAccountController(AccountController accountController) {
        this.accountController = accountController;
    }

    @Inject
    public void setTransferController(TransferController transferController) {
        this.transferController = transferController;
    }

    public void start() {

        path("/users", () -> {
            get("", userController::getAll, GSON::toJson);
            path("/:id", () -> {
                get("", userController::getById, GSON::toJson);
                get("/accounts", accountController::getByUserId, GSON::toJson);
            });
        });

        post("/transfer", MIME_JSON, transferController::transfer, GSON::toJson);

        afterAfter((req, resp) -> resp.type(MIME_JSON));

        exception(Exception.class, (e, req, resp) -> {
            String errMsg = "Internal server error";
            log.error(errMsg, e);
            resp.body(GSON.toJson(new RespObj(ERROR, errMsg)));
            resp.status(500);
        });
    }

}
