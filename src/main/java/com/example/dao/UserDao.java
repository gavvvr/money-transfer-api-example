package com.example.dao;

import com.example.model.User;
import lombok.SneakyThrows;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Singleton
public class UserDao extends DaoBase {

    @Inject
    public UserDao(DataSource ds) {
        super(ds);
    }

    @SneakyThrows
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        String query = "select user_id, user_name from user";
        try (Connection conn = ds.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    users.add(new User(rs.getLong("user_id"), rs.getString("user_name")));
                }
            }
        }
        return users;
    }

    @SneakyThrows
    public Optional<User> findById(long id) {
        String query = "select user_id, user_name from user where user_id = ?";
        try (Connection conn = ds.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
            st.setLong(1, id);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(new User(id, rs.getString("user_name")));
                } else {
                    return Optional.empty();
                }
            }
        }
    }

}
