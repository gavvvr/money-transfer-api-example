package com.example.dao;

import dagger.Module;
import dagger.Provides;
import lombok.SneakyThrows;
import org.h2.jdbcx.JdbcConnectionPool;
import org.h2.tools.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Module
public class DbModule {

    private static final Logger log = LoggerFactory.getLogger(DbModule.class);
    public static final String H2_DEFAULT_USER = "sa";
    public static final String H2_DEFAULT_PASSWORD = "";
    public static final String JDBC_URL = "jdbc:h2:mem:test";


    @Provides
    @Singleton
    @SneakyThrows
    static DataSource provideDs() {
        String initUrl =
                JDBC_URL + ";INIT=runscript from 'classpath:sql/schema.sql'" +
                        "\\;runscript from 'classpath:sql/data.sql'";

        JdbcConnectionPool cp = JdbcConnectionPool.create(JDBC_URL, H2_DEFAULT_USER, H2_DEFAULT_PASSWORD);
        createDbByOpeningConnection(cp);
        initDb(initUrl);
        if (System.getProperty("db.open.browser") != null) {
            startWebServerFrom(JDBC_URL);
        }
        startWebServer();
        startTcpServer();

        return cp;
    }

    @SneakyThrows
    private static void initDb(String jdbcUrl) {
        try (Connection ignored = DriverManager.getConnection(jdbcUrl, H2_DEFAULT_USER, H2_DEFAULT_PASSWORD)) {
            log.info("Database has been initialized");
        }
    }

    @SneakyThrows
    private static void createDbByOpeningConnection(DataSource ds) {
        try (Connection ignored = ds.getConnection()) {
            log.info("Database has been created in memory");
        }
    }

    private static void startWebServerFrom(String connStr) {
        Thread serverDaemon = new Thread(() -> {
            try (Connection conn = DriverManager.getConnection(connStr, H2_DEFAULT_USER, H2_DEFAULT_PASSWORD)) {
                Server.startWebServer(conn);
            } catch (SQLException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        });
        serverDaemon.setDaemon(true);
        serverDaemon.start();
    }

    private static void startWebServer() {
        try {
            Server.createWebServer(
                    "-web", "-webAllowOthers",
                    "-webPort", "8083")
                  .start();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static void startTcpServer() {
        try {
            Server.createTcpServer(
                    "-tcp", "-tcpAllowOthers",
                    "-tcpPort", "9093")
                  .start();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
