package com.example.dao;

import com.example.model.Account;
import com.example.model.Currency;
import lombok.SneakyThrows;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class AccountDao extends DaoBase {
    @Inject
    public AccountDao(DataSource ds) {
        super(ds);
    }

    @SneakyThrows
    public List<Account> findByUserId(long id) {
        List<Account> accounts = new ArrayList<>();
        String query = "select account_id as id, account_name, currency, balance from account where user_id = ?";
        try (Connection conn = ds.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
            st.setLong(1, id);
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    accounts.add(new Account(
                                    rs.getLong("id"),
                                    rs.getString("account_name"),
                                    Currency.valueOf(rs.getString("currency")),
                                    rs.getBigDecimal("balance")
                            )
                    );
                }
            }
        }
        return accounts;
    }

}
