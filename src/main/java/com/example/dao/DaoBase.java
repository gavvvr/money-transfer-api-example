package com.example.dao;

import javax.sql.DataSource;

public abstract class DaoBase {
    final DataSource ds;

    public DaoBase(DataSource ds) {
        this.ds = ds;
    }
}
