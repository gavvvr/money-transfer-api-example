package com.example;

import com.example.web.DaggerWebApp;

public class App {

    public static void main(String[] args) {
        DaggerWebApp.create()
                    .router()
                    .start();
    }
}
