package com.example.controller;

import com.example.dto.TransferPost;
import com.example.model.Account;
import com.example.model.Currency;
import com.example.model.TransferLog;
import com.example.web.RespObj;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;
import java.util.Optional;

import static com.example.Utils.GSON;
import static com.example.web.RespObj.Result.ERROR;
import static com.example.web.RespObj.Result.SUCCESS;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

public class TransferController {

    private static final Logger log = LoggerFactory.getLogger(TransferController.class);

    private final DataSource ds;

    @Inject
    public TransferController(DataSource ds) {
        this.ds = ds;
    }


    public RespObj transfer(Request req, Response resp) {
        log.info("Received transfer request: {}", req.body());
        TransferPost transferReq = GSON.fromJson(req.body(), TransferPost.class);
        if (transferReq.amount.compareTo(BigDecimal.ZERO) <= 0) {
            String errMsg = "The amount is expected to be greater than 0, but was provided: " + transferReq.amount;
            return new RespObj(ERROR, errMsg);
        }
        return tryTransfer(transferReq, resp);
    }

    @SneakyThrows
    RespObj tryTransfer(TransferPost tReq, Response resp) {
        long srcId = tReq.fromId;
        long dstId = tReq.toId;
        try (Connection conn = ds.getConnection()) {
            conn.setAutoCommit(false);
            Optional<Account> oSrcAcc = selectForUpdateAccount(srcId, conn);
            if (!oSrcAcc.isPresent()) {
                resp.status(HTTP_NOT_FOUND);
                return new RespObj(ERROR, "Source account with id = " + srcId + " does not exist");
            }
            Account srcAccc = oSrcAcc.get();
            if (srcAccc.getBalance().compareTo(tReq.amount) < 0) {
                resp.status(HTTP_BAD_REQUEST);
                return new RespObj(ERROR, "Not enough balance on account with id = " + srcId + ". " +
                        "Balance is = " + srcAccc.getBalance() + ". Required: " + tReq.amount);
            }

            Optional<Account> oDstAcc = selectForUpdateAccount(dstId, conn);
            if (!oDstAcc.isPresent()) {
                resp.status(HTTP_NOT_FOUND);
                return new RespObj(ERROR, "Destination account with id = " + dstId + " does not exist");
            }
            Account dstAcc = oDstAcc.get();

            if (srcAccc.currency != dstAcc.currency) {
                resp.status(HTTP_NOT_FOUND);
                return new RespObj(ERROR, "Can't transfer money from account with currency: " + srcAccc.currency +
                        " to account with currency: " + dstAcc.currency);
            }

            srcAccc.withdraw(tReq.amount);
            dstAcc.deposit(tReq.amount);

            updateAccountBalance(srcAccc, conn);
            updateAccountBalance(dstAcc, conn);

            TransferLog transferLog = TransferLog.createNew(srcId, dstId, tReq.amount);
            writeTransactionLog(transferLog, conn);

            conn.commit();
            return new RespObj(SUCCESS, tReq);
        }
    }

    void writeTransactionLog(TransferLog txLog, Connection conn) throws SQLException {
        try (PreparedStatement ps = conn.prepareStatement(
                "insert into TRANSFER_LOG (FROM_ID, TO_ID, AMOUNT, STAMP) values (?, ?, ?, ?)"
        )) {
            ps.setLong(1, txLog.from_id);
            ps.setLong(2, txLog.to_id);
            ps.setBigDecimal(3, txLog.amount);
            ps.setTimestamp(4, Timestamp.from(txLog.stamp));

            ps.executeUpdate();
        }
    }

    void updateAccountBalance(Account acc, Connection conn) throws SQLException {
        try (PreparedStatement ps = conn.prepareStatement("update ACCOUNT set BALANCE = ? where ACCOUNT_ID = ?")) {
            ps.setBigDecimal(1, acc.getBalance());
            ps.setLong(2, acc.accountId);
            if (ps.executeUpdate() == 0) {
                log.error("Unable to update account(id={}) balance to a value: {}", acc.accountId, acc.getBalance());
                throw new RuntimeException("Failed to update account balance");
            }
        }
    }

    Optional<Account> selectForUpdateAccount(long id, Connection conn) throws SQLException {
        try (PreparedStatement st = conn.prepareStatement(selectForUpdateAccountQuery(id))) {
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(new Account(
                            id,
                            rs.getString("ACCOUNT_NAME"),
                            Currency.valueOf(rs.getString("CURRENCY")),
                            rs.getBigDecimal("BALANCE")
                    ));
                } else {
                    return Optional.empty();
                }
            }
        }
    }

    String selectForUpdateAccountQuery(long id) {
        return "select ACCOUNT_NAME, CURRENCY, BALANCE from ACCOUNT where ACCOUNT_ID = " + id + " for update";
    }
}
