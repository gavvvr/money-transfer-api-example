package com.example.controller;

import com.example.dao.AccountDao;
import com.example.web.RespObj;
import spark.Request;
import spark.Response;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;

import static com.example.controller.UserController.invalidUserIdResp;
import static com.example.Utils.parseLong;
import static com.example.web.RespObj.Result.SUCCESS;

@Singleton
public class AccountController {

    private final AccountDao accountDao;

    @Inject
    public AccountController(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public RespObj getByUserId(Request req, Response resp) {
        String userIdParam = req.params(":id");

        Optional<Long> oUid = parseLong(userIdParam);
        return oUid.map(aLong -> new RespObj(SUCCESS, accountDao.findByUserId(aLong)))
                   .orElseGet(() -> invalidUserIdResp(userIdParam, resp));
    }
}
