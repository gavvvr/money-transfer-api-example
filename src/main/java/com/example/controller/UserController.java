package com.example.controller;

import com.example.dao.UserDao;
import com.example.web.RespObj;
import spark.Request;
import spark.Response;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;

import static com.example.Utils.parseLong;
import static com.example.web.RespObj.Result.ERROR;
import static com.example.web.RespObj.Result.SUCCESS;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

@Singleton
public class UserController {

    private final UserDao userDao;

    @Inject
    public UserController(UserDao userDao) {
        this.userDao = userDao;
    }

    public RespObj getById(Request req, Response resp) {
        String idParam = req.params(":id");

        Optional<Long> oUid = parseLong(idParam);
        if (!oUid.isPresent()) {
            return invalidUserIdResp(idParam, resp);
        }

        return oUid.flatMap(userDao::findById)
                   .map(u -> new RespObj(SUCCESS, u))
                   .orElseGet(() -> userNotExistsResp(oUid.get(), resp));
    }

    public RespObj getAll(Request req, Response resp) {
        return new RespObj(SUCCESS, userDao.findAll());
    }

    public static RespObj invalidUserIdResp(String idParam, Response resp) {
        resp.status(HTTP_BAD_REQUEST);
        return new RespObj(ERROR, "Could not parse user id from request parameter: " + idParam);
    }

    public static RespObj userNotExistsResp(long id, Response resp) {
        resp.status(HTTP_NOT_FOUND);
        return new RespObj(ERROR, "User with id=" + id + " does not exists");
    }
}
