package com.example.dto;

import lombok.ToString;

import java.math.BigDecimal;

@ToString
public class TransferPost {
    public final long fromId;
    public final long toId;
    public final BigDecimal amount;

    public TransferPost(long fromId, long toId, BigDecimal amount) {
        this.fromId = fromId;
        this.toId = toId;
        this.amount = amount;
    }
}
