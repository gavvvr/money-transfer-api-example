package com.example;

import com.google.gson.Gson;

import java.util.Optional;

public class Utils {

    public static final Gson GSON = new Gson();

    public static Optional<Long> parseLong(String str) {
        try {
            return Optional.of(Long.parseLong(str));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }
}
