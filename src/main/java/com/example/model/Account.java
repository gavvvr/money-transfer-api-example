package com.example.model;

import java.math.BigDecimal;

public class Account {
    public long accountId;
    public String accountName;
    public final Currency currency;
    private BigDecimal balance;

    public Account(long accountId, String accountName, Currency currency, BigDecimal balance) {
        this.accountId = accountId;
        this.accountName = accountName;
        this.currency = currency;
        this.balance = balance;
    }

    public void withdraw(BigDecimal amount) {
        balance = balance.subtract(amount);
    }

    public void deposit(BigDecimal amount) {
        balance = balance.add(amount);
    }

    public BigDecimal getBalance() {
        return balance;
    }
}
