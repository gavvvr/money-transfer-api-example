package com.example.model;

import java.math.BigDecimal;
import java.time.Instant;

public class TransferLog {
    public final long from_id;
    public final long to_id;
    public final BigDecimal amount;
    public final Instant stamp;

    public TransferLog(long from_id, long to_id, BigDecimal amount, Instant stamp) {
        this.from_id = from_id;
        this.to_id = to_id;
        this.amount = amount;
        this.stamp = stamp;
    }

    public static TransferLog createNew(long from_id, long to_id, BigDecimal amount) {
        return new TransferLog(from_id, to_id, amount, Instant.now());
    }
}
