package com.example.model;

import java.util.List;

public class User {
    private long userId;
    private String name;
    private List<Account> accounts;

    public User(long userId, String name) {
        this.userId = userId;
        this.name = name;
    }
}
