Revolut backend test
===

A simple implementation of money transfer REST API 
reliably handling multiple concurrent HTTP requests.

Technology stack:

* Java 8
* Dagger 2
* Spark Framework
* GSON
* H2 in-memory database
* Lombok (mostly to `@SneakyThrow`ing checked SQL exceptions)

#### model and API

The money can be transferred using `POST` request on `/transfer`
with the following JSON payload:

```json
{
  "fromId": 3,
  "toId": 5,
  "amount": 9.99
}
```

where `fromId` and `toId` is an id of source and destination account.

According to model a user (`/users`) can have multiple accounts (`/users/:id/accounts`).
For simplicity the API can transfer money only between accounts of the same currency.

#### Building

> ./gradlew clean shadowJar

#### Running

> java -jar build/libs/revolut-test-api-all.jar

or

> ./gradlew run

The app will be available at http://localhost:4567/

#### Working with database

Embedded in-memory h2 database instance is used.

While the app is running, you can access it:
 
* either with WEB gui located at: http://localhost:8083
* or with your database tool using `jdbc:h2:tcp://localhost:9093/mem:test` jdbc URL

Credentials are default: `sa` user with empty password.

*The database gets initialized on startup with schema and sample data from classpath resources*
